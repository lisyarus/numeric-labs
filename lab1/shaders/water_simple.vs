#version 330

uniform mat4 projection;
uniform mat4 modelview;

layout (location = 0) in vec2 static_in;
layout (location = 1) in float height_in;
layout (location = 2) in vec3 normal_in;

out vec3 vertex;
out vec3 normal;

void main ( )
{
	vertex = vec3(static_in, height_in);
	normal = normal_in;
	gl_Position = projection * modelview * vec4(static_in, height_in, 1.0);
}
