#version 330

uniform float dx;
uniform float dt;
uniform float middle;

layout (location = 0) in float h_in;
layout (location = 1) in float h_vel_in;

layout (location = 2) in float hx0_in;
layout (location = 3) in float hx1_in;
layout (location = 4) in float hy0_in;
layout (location = 5) in float hy1_in;

layout (location = 6) in float edge_in;

out float h;
out float h_vel;

void main ( )
{
	if (edge_in < 0.5)
	{
		h_vel = h_vel_in;
		h = h_in;
	}
	else
	{
		float L = (hx0_in + hx1_in + hy0_in + hy1_in - 4.0 * h_in) / dx / dx;
		h_vel = h_vel_in + 0.15 * dt * L;
		h = h_in + h_vel * dt - (h_in - middle)*0.003;
	}
}
