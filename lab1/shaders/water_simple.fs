#version 330

uniform vec4 light;
uniform vec4 color;

in vec3 vertex;
in vec3 normal;

void main ( )
{
	vec3 n = normalize(normal);
	float brightness = max(0.0, dot(n, normalize(light.xyz - light.w * vertex))) * 0.7 + 0.3;
	gl_FragColor = color * brightness;
}
