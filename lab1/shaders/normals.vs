#version 330

layout (location = 0) in float h_in;
layout (location = 1) in vec2 p_in;

layout (location = 2) in float hx0_in;
layout (location = 3) in vec2 px0_in;

layout (location = 4) in float hx1_in;
layout (location = 5) in vec2 px1_in;

layout (location = 6) in float hy0_in;
layout (location = 7) in vec2 py0_in;

layout (location = 8) in float hy1_in;
layout (location = 9) in vec2 py1_in;

layout (location = 10) in float edge_in;

out vec3 n;

void main ( )
{
	if (edge_in < 0.5)
	{
		n = vec3(0.0, 0.0, 1.0);
	}
	else
	{
		vec3 p = vec3(p_in, h_in);
		vec3 px0 = vec3(px0_in, hx0_in);
		vec3 px1 = vec3(px1_in, hx1_in);
		vec3 py0 = vec3(py0_in, hy0_in);
		vec3 py1 = vec3(py1_in, hy1_in);

		vec3 n00 =  normalize(cross(px0 - p, py0 - p));
		vec3 n10 = -normalize(cross(px1 - p, py0 - p));
		vec3 n01 = -normalize(cross(px0 - p, py1 - p));
		vec3 n11 =  normalize(cross(px1 - p, py1 - p));

		n = normalize((n00 + n10 + n01 + n11) / 4.0);
	}
}
