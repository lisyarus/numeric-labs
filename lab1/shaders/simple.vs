#version 330

uniform mat4 transform;

layout (location = 0) in vec3 vertex_in;

void main ( )
{
	gl_Position = transform * vec4(vertex_in, 1.0);
}
