#version 330

uniform vec2 center;
uniform float radius;
uniform float intensity;
uniform float middle;
uniform float mode;

layout (location = 0) in float h_in;
layout (location = 1) in float h_vel_in;
layout (location = 2) in vec2 pos_in;
layout (location = 3) in float edge_in;

out float h;
out float h_vel;

void main ( )
{
	if (edge_in > 0.5)
	{
		vec2 d = (pos_in - center) / radius;
		if (mode == 0.0)
		{
			h = h_in + exp(- dot(d,d)) * intensity / (1.0 + pow(h_in - middle, 2));
		}
		else if (mode == 1.0)
		{
			h = h_in + exp(- dot(d,d)) * intensity / (1.0 + pow(h_in - middle, 2)) * mix(1.0, sin(atan(d.y,d.x) * 8.0), max(0.0, length(d) - 0.4));
		}
	}
	else
	{
		h = h_in;
	}
	h_vel = h_vel_in;
}
