#version 330

uniform sampler2D atlas;
uniform vec4 color;

in vec2 texcoord;

void main ( )
{
	vec4 c = color;
	c.a *= texture2D(atlas, texcoord).r;
	gl_FragColor = c;
}
