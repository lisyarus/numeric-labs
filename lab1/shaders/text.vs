#version 330

uniform mat4 projection;

layout (location = 0) in vec2 position_in;
layout (location = 1) in vec2 texcoord_in;

out vec2 texcoord;

void main ( )
{
	gl_Position = projection * vec4(position_in, 0.0, 1.0);
	texcoord = texcoord_in;
}
