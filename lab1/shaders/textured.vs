#version 330
uniform mat4 projection;
uniform mat4 modelview;

layout (location = 0) in vec3 vertex_in;
layout (location = 1) in vec3 normal_in;
layout (location = 2) in vec2 texcoord_in;

out vec3 vertex;
out vec3 normal;
out vec2 texcoord;

void main ( )
{
	gl_Position = projection * modelview * vec4(vertex_in, 1);
	vertex = vertex_in;
	normal = normal_in;
	texcoord = texcoord_in;
}
