#version 330

uniform vec4 light;
uniform vec3 camera;
uniform sampler2D floor_texture;
uniform sampler2D wall_texture;
uniform sampler2D caustics_texture;
uniform vec3 origin;
uniform vec3 size;

in vec3 vertex;
in vec3 normal;

vec3 refract (vec3 i, vec3 n, float f)
{
	float d = dot(i, n);
	// Just 'd' does not work, probably due to precision issues, thus 'sqrt(d*d)'
	return i * f + (sqrt(d*d) * f - sqrt(1 - f*f*(1 - d*d))) * n;
}

vec4 raycast (vec3 v, vec3 r, float deep_factor)
{
	float t = (origin.z - v.z) / r.z;
	vec3 p = v + r * t;
	vec3 surface_normal;
	bool hit_surface = false;
	bool use_caustics = false;
	vec4 color = vec4(0.9, 0.95, 1.0, 0.0);
	if (t >= 0 && p.x > origin.x && p.x < origin.x + size.x && p.y > origin.y && p.y < origin.y + size.y)
	{
		color = texture2D(floor_texture, p.xy - origin.xy);
		surface_normal = vec3(0.0, 0.0, 1.0);
		hit_surface = true;
		use_caustics = true;
	}
	else
	{
		vec2 tt;
		if (r.x > 0)
		{
			tt.x = (origin.x + size.x - v.x) / r.x;
		}
		else
		{
			tt.x = (origin.x - v.x) / r.x;
		}
		if (r.y > 0)
		{
			tt.y = (origin.y + size.y - v.y) / r.y;
		}
		else
		{
			tt.y = (origin.y - v.y) / r.y;
		}

		if (tt.x < tt.y)
		{
			p = v + tt.x * r;
			if (p.z > origin.z && p.z < origin.z + size.z)
			{
				color = texture2D(wall_texture, p.yz - origin.yz);
				surface_normal = vec3(-sign(r.x), 0.0, 0.0);
				hit_surface = true;
				t = tt.x;
			}
		}
		else
		{
			p = v + tt.y * r;
			if (p.z > origin.z && p.z < origin.z + size.z)
			{
				color = texture2D(wall_texture, p.xz - origin.xz);
				surface_normal = vec3(0.0, -sign(r.y), 0.0);
				hit_surface = true;
				t = tt.y;
			}
		}
	}

	if (hit_surface)
	{
		float brightness = max(0.0, dot(surface_normal, normalize(light.xyz - light.w * p))) * 0.7 + 0.3;
		color.xyz = color.xyz * brightness;

		if (use_caustics)
		{
			float caust = textureLod(caustics_texture, (p.xy - origin.xy) / size.xy, 1.0).r;
			color.xyz = color.xyz + vec3(1.0, 1.0, 1.0) * caust;
		}

		color = mix(color, vec4(0.0, 0.2, 0.5, 1.0), sqrt(min(t * deep_factor, 1.0)));
	}

	return color;
}

void main ( )
{
	vec3 n = normalize(normal);
	vec3 to_light = normalize(light.xyz - light.w * vertex);
	vec3 to_camera = normalize(camera - vertex);
	vec3 light_reflection = 2 * n * dot(n, to_light) - to_light;
	float specular = pow(max(0.0, dot(light_reflection, to_camera)), 30);

	bool up = dot(to_camera, n) < 0.0;

	vec3 r;
	// Refraction
	r = refract(- to_camera, n, 0.75);
	vec4 refr_color = raycast(vertex, r, up ? 0.0 : 0.5);

	// Reflection
	r = 2 * normal * dot(n, to_camera) - to_camera;
	vec4 refl_color = raycast(vertex, r, 0.0);

	vec4 color = mix(refl_color, refr_color, pow(max(0.0, abs(dot(n, to_camera))), 0.2));

	color.xyz = color.xyz + vec3(1.0, 1.0, 1.0) * specular;

	gl_FragColor = color;
}
