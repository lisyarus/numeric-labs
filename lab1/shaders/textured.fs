#version 330

uniform vec4 light;
uniform sampler2D texture;
uniform sampler2D caustics_texture;
uniform bool use_caustics;
uniform vec3 origin;
uniform vec3 size;

in vec3 vertex;
in vec3 normal;
in vec2 texcoord;

void main ( )
{
	vec3 n = normalize(normal);
	float brightness = max(0.0, dot(n, normalize(light.xyz - light.w * vertex))) * 0.7 + 0.3;
	float caust = 0.0;
	if (use_caustics)
		caust = textureLod(caustics_texture, (vertex.xy - origin.xy)/size.xy, 1.0).r;
	gl_FragColor = texture2D(texture, texcoord) * brightness + vec4(1.0, 1.0, 1.0, 1.0) * caust;
}
