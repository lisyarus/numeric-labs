#version 330

uniform vec4 light;
uniform vec3 origin;
uniform vec3 size;

layout (location = 0) in vec2 static_in;
layout (location = 1) in float height_in;
layout (location = 2) in vec3 normal_in;

vec3 refract (vec3 i, vec3 n, float f)
{
	float d = dot(i, n);
	// Just 'd' does not work, probably due to precision issues, thus 'sqrt(d*d)'
	return i * f + (sqrt(d*d) * f - sqrt(1 - f*f*(1 - d*d))) * n;
}

void main ( )
{
	vec3 vertex = vec3(static_in, height_in);
	vec3 r = normalize(vertex * light.w - light.xyz);
	r = refract(r, normal_in, 0.75);

	float t = (origin.z - vertex.z) / r.z;

	gl_Position = vec4((vertex.xy + t * r.xy - origin.xy) / size.xy / 0.5 - vec2(1.0, 1.0), 0.0, 1.0);
}
