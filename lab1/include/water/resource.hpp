#pragma once

#include <string>
#include <vector>

#include <xgl/objects/program.hpp>

std::string load_file (std::string const & name);

template <typename Shader>
Shader load_shader (std::string const & name)
{
	return Shader::create(load_file(name));
}

xgl::program load_program (std::string const & name);

xgl::program load_transform_program (std::string const & name, std::vector<std::string> const & capture, xgl::enum_t attribs = GL_SEPARATE_ATTRIBS);
