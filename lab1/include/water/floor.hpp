#pragma once

#include <water/textured_triangles.hpp>

struct floor_renderer
	: textured_triangles_renderer
{
	floor_renderer ( );

	floor_renderer (geom::rectangle_3f const & box)
		: floor_renderer()
	{
		update(box);
	}

	void update (geom::rectangle_3f const & box);

private:
	geom::rectangle_3f box_;

	void set_uniforms (xgl::program & program) override;
};
