#pragma once

#include <xgl/objects/texture.hpp>
#include <xgl/objects/program.hpp>
#include <xgl/objects/array.hpp>
#include <xgl/objects/buffer.hpp>

struct textured_triangles_renderer
{
	xgl::texture_2d const * caustics_texture;

	textured_triangles_renderer ( );

	void render (geom::matrix_4x4f const & projection, geom::matrix_4x4f const & modelview, geom::vector_4f const & light, geom::point_3f const & camera);

	xgl::texture_2d const & texture ( )
	{
		return texture_;
	}

protected:

	struct vertex
	{
		geom::point_3f position;
		geom::vector_3f normal;
		geom::vector_2f texcoords;
	};

	void set_data (vertex const * data, std::size_t size);

	virtual void set_uniforms (xgl::program & program) { }

protected:
	xgl::texture_2d texture_;

private:
	xgl::program program_;
	xgl::array array_;
	xgl::array_buffer buffer_;
	GLuint count_;
};
