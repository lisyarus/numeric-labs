#pragma once

#include <xgl/objects/program.hpp>
#include <xgl/objects/framebuffer.hpp>
#include <xgl/objects/texture.hpp>

#include <functional>

struct caustics_renderer
{
	std::function<void()> vertex_renderer;

	caustics_renderer (geom::rectangle_3f const & box);

	xgl::texture_2d const & texture ( ) const
	{
		return texture_;
	}

	void render (geom::vector_4f const & light);

	void clear ( );

private:
	xgl::program program_;
	xgl::framebuffer framebuffer_;
	xgl::texture_2d texture_;
	geom::rectangle_3f box_;
};
