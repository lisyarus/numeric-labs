#pragma once

#include <geom/alias/vector.hpp>
#include <geom/alias/point.hpp>
#include <geom/alias/matrix.hpp>

template <typename T>
struct smooth_value
{
	T value;
	T value_target;
	T smoothness;

	static constexpr T default_smoothness = static_cast<T>(0.7);

	smooth_value (T value = T(), T smoothness = default_smoothness)
		: value(value)
		, value_target(value)
		, smoothness(smoothness)
	{ }

	void step (T dt = T(1))
	{
		value += (value_target - value) * dt * smoothness;
	}

	T operator * ( ) const
	{
		return value;
	}
};

struct camera
{
	smooth_value<float> a, b;
	smooth_value<float> d;
	geom::point_3f target;

	camera ( );

	geom::matrix_4x4f transform ( ) const;

	geom::point_3f position ( ) const;

	void on_wheel (float delta);

	void on_rotate (geom::vector_2i delta);

	void step (float dt = 1.f)
	{
		a.step(dt);
		b.step(dt);
		d.step(dt);
	}
};
