#pragma once

#include <xgl/objects/texture.hpp>
#include <xgl/objects/program.hpp>
#include <xgl/objects/array.hpp>
#include <xgl/objects/buffer.hpp>

#include <functional>

struct water_renderer
{
	xgl::texture_2d const * floor_texture;
	xgl::texture_2d const * wall_texture;
	xgl::texture_2d const * caustics_texture;

	water_renderer ( );

	water_renderer (geom::rectangle_3f const & box, GLuint size)
		: water_renderer()
	{
		update(box, size);
	}

	std::function<void()> make_vertex_renderer ( );

	void render (geom::matrix_4x4f const & projection, geom::matrix_4x4f const & modelview, geom::vector_4f const & light, geom::point_3f const & camera);
	void render_simple (geom::matrix_4x4f const & projection, geom::matrix_4x4f const & modelview, geom::vector_4f const & light, geom::point_3f const & camera);

	void update (geom::rectangle_3f const & box, GLuint size);

	void step ( );

	void add (geom::point_2f const & center, float radius, float intensity, int mode);

	void clear ( );

	void load_steady_state (int sx, int sy);

private:
	xgl::program program_;
	xgl::program simple_program_;
	xgl::program step_program_;
	xgl::program normals_program_;
	xgl::program add_program_;
	xgl::array array_;
	xgl::array step_array_;
	xgl::array_buffer position_buffer_;
	xgl::array_buffer edge_buffer_;
	xgl::array_buffer height_buffer_;
	xgl::array_buffer height_buffer_back_;
	xgl::array_buffer height_vel_buffer_;
	xgl::array_buffer height_vel_buffer_back_;
	xgl::array_buffer normal_buffer_;
	xgl::element_array_buffer index_buffer_;

	GLuint size_;
	GLuint sizex_, sizey_;

	geom::rectangle_3f box_;

	std::size_t index_count ( );
	std::size_t vertex_count ( );

	void update_normals ( );
};
