#pragma once

#ifdef LAB1_USE_TEXT

#include <xgl/objects/program.hpp>
#include <xgl/text/mesh.hpp>
#include <xgl/text/glyph.hpp>

struct text_renderer
{
	text_renderer ( );

	void update (std::string const & text, geom::point_2f const & start = {0.f, 0.f});

	void render (geom::matrix_4x4f const & projection);

private:
	xgl::program program_;
	xgl::text::mesh mesh_;
	xgl::text::glyph_atlas atlas_;
};

#else

struct text_renderer
{
	void update (std::string const & text, geom::point_2f const & start = {0.f, 0.f}) { }

	void render (geom::matrix_4x4f const & projection) { }
};

#endif
