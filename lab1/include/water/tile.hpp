#pragma once

#include <xgl/objects/texture.hpp>
#include <xgl/pixel.hpp>

xgl::texture_2d create_tile_texture (xgl::pixel_4b c1, xgl::pixel_4b c2);
