#pragma once

#include <water/textured_triangles.hpp>

struct walls_renderer
	: textured_triangles_renderer
{
	walls_renderer ( );

	walls_renderer (geom::rectangle_3f const & box)
		: walls_renderer()
	{
		update(box);
	}

	void update (geom::rectangle_3f const & box);
};
