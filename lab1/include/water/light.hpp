#pragma once

#include <xgl/objects/program.hpp>
#include <xgl/objects/array.hpp>
#include <xgl/objects/buffer.hpp>

struct light_renderer
{
	geom::point_3f position;
	float size;

	light_renderer ( );

	void render (geom::matrix_4x4f const & projection, geom::matrix_4x4f const & modelview);

private:
	xgl::program program_;
	xgl::array array_;
	xgl::array_buffer buffer_;
};
