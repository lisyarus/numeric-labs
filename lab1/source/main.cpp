#include <water/camera.hpp>
#include <water/light.hpp>
#include <water/floor.hpp>
#include <water/walls.hpp>
#include <water/water.hpp>
#include <water/caustics.hpp>
#include <water/text.hpp>

#include <xsdl/init.hpp>
#include <xsdl/opengl.hpp>
#include <xsdl/event_poller.hpp>
#include <SDL2/SDL_video.h>

#include <xgl/state/viewport.hpp>
#include <xgl/state/enable.hpp>
#include <xgl/error.hpp>

#include <geom/transform/perspective.hpp>
#include <geom/transform/homogeneous.hpp>
#include <geom/transform/scale.hpp>
#include <geom/transform/translation.hpp>
#include <geom/operations/matrix.hpp>
#include <geom/operations/interval.hpp>
#include <geom/math.hpp>

#include <util/to_string.hpp>
#include <util/autoname.hpp>
#include <util/clock.hpp>
#include <util/moving_average.hpp>

#include <iostream>

struct fps_counter
{
	fps_counter ( )
		: avg_(10)
	{ }

	void reset ( )
	{
		timer_.restart();
		avg_.clear();
	}

	void frame ( )
	{
		avg_.push(timer_.count());
		timer_.restart();
	}

	double fps ( ) const
	{
		return 1.0 / avg_.average();
	}

private:
	util::clock<> timer_;
	util::moving_average<float> avg_;
};

void toggle (bool & flag)
{
	flag = !flag;
}

int main (int argc, char ** argv)
{
	if (argc != 1 && argc != 4 && argc != 6)
	{
		std::cout << "Usage: " << argv[0] << " [ size_x size_y size_z [ steady_x steady_y ] ]\n";
		return 0;
	}

	float size_x = 10.f;
	float size_y = 6.f;
	float size_z = 3.f;

	bool init_steady = false;
	int steady_x, steady_y;

	if (argc >= 4)
	{
		size_x = util::from_string<float>(argv[1]);
		size_y = util::from_string<float>(argv[2]);
		size_z = util::from_string<float>(argv[3]);
	}

	if (argc >= 6)
	{
		init_steady = true;
		steady_x = util::from_string<float>(argv[4]);
		steady_y = util::from_string<float>(argv[5]);
	}

	xsdl::initializer_t autoname;

	xsdl::event_poller_t poller;
	xsdl::gl_window_t window(poller, "xgl test", SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN);

	geom::point_2i mouse;

	auto context = xgl::make_global_context(xgl::make_cached_state());
	context->make_current();

	geom::rectangle_3f box {{
		{-size_x/2.f, size_x/2.f},
		{-size_y/2.f, size_y/2.f},
		{0.f, size_z},
	}};

	camera cam;
	cam.target = box(0.5f, 0.5f, 0.5f);
	geom::matrix_4x4f projection;
	geom::matrix_4x4f projection_2d;

	light_renderer light;
	light.position = box(0.25f, 0.25f, 0.95f);
	light.size = 0.1f;

	floor_renderer floor(box);
	walls_renderer walls(box);

	water_renderer water(box, 32);
	water.floor_texture = &floor.texture();
	water.wall_texture = &walls.texture();
	if (init_steady)
		water.load_steady_state(steady_x, steady_y);

	caustics_renderer caustics(box);
	caustics.vertex_renderer = water.make_vertex_renderer();
	floor.caustics_texture = &caustics.texture();
	water.caustics_texture = &caustics.texture();

	text_renderer info_text;
	text_renderer helper_text;
	helper_text.update("Click to emit wave", {20.f, 20.f});

	bool show_helper = false;
	bool force_show_helper = true;

	geom::point_2f mouse_proj { 0.f, 0.f };

	bool quit = false;
	bool paused = false;

	bool show_light = false;
	bool show_water = true;

	bool water_simple = false;

	fps_counter fps;

	auto update_info_text = [&]
	{
		std::ostringstream oss;
		oss << (paused ? "Paused" : "Running") << " [Space]\n";
		oss << "Water: " << (show_water ? "shown" : "hidden") << " [W], " << (water_simple ? "simple" : "realistic") << " [S]\n";
		oss << "Light: " << (show_light ? "shown" : "hidden") << " [L]\n";
		oss << "Helper text: " << (force_show_helper ? "shown" : "hidden") << " [H]\n";
		oss << "FPS: " << fps.fps() << "\n";
		info_text.update(oss.str(), {5.f, 5.f});
	};

	update_info_text();

	auto update_mouse_proj = [&]
	{
		geom::vector_4f camera_screen { 0.f, 0.f, 0.f, 1.f };

		geom::vector_4f mouse_screen;
		mouse_screen[0] = float(mouse[0]) / window.size().width * 2.f - 1.f;
		mouse_screen[1] = 1.f - float(mouse[1]) / window.size().height * 2.f;
		mouse_screen[2] = 0.f;
		mouse_screen[3] = 1.f;

		geom::matrix_4x4f inverse_modelview = geom::inverse(cam.transform());
		geom::matrix_4x4f inverse_projection_modelview = geom::inverse(projection * cam.transform());

		auto camera_world_4 = inverse_modelview * camera_screen;
		camera_world_4 /= camera_world_4[3];

		auto mouse_world_4 = inverse_projection_modelview * mouse_screen;
		mouse_world_4 /= mouse_world_4[3];

		geom::point_3f camera_world, mouse_world;

		for (std::size_t i = 0; i < 3; ++i)
		{
			camera_world[i] = camera_world_4[i];
			mouse_world[i] = mouse_world_4[i];
		}

		geom::vector_3f direction = geom::normalized(mouse_world - camera_world);

		float t = (box[2](0.5f) - camera_world[2]) / direction[2];

		geom::point_3f proj = camera_world + direction * t;
		mouse_proj[0] = proj[0];
		mouse_proj[1] = proj[1];

		show_helper = geom::contains(box[0], mouse_proj[0]) && geom::contains(box[1], mouse_proj[1]);
	};

	poller.on_quit([&quit]{ quit = true; });

	poller.on_key_down([&](auto, std::int32_t k)
	{
		if (k == SDLK_ESCAPE)
			quit = true;

		if (k == SDLK_SPACE) toggle(paused);

		if (k == SDLK_l) toggle(show_light);

		if (k == SDLK_w) toggle(show_water);

		if (k == SDLK_s) toggle(water_simple);

		if (k == SDLK_h) toggle(force_show_helper);

		if (k == SDLK_c)
			water.clear();

		update_info_text();
	});

	poller.on_resize([&](auto, xsdl::window_t::size_t size)
	{
		geom::rectangle_2i viewport {{
			{0, size.width},
			{0, size.height},
		}};
		xgl::viewport(viewport);

		projection = geom::perspective<float, 3>(geom::pi / 3.f, float(size.width) / size.height, 0.01f, 1000.f).homogeneous_matrix();

		projection_2d = geom::matrix_4x4f::identity()
				* geom::scale<float, 4>({1.f, -1.f, 1.f, 1.f}).matrix()
				* geom::translation<float, 3>({-1.f, -1.f, 0.f}).homogeneous_matrix()
				* geom::scale<float, 4>({2.f / size.width, 2.f / size.height, 1.f, 1.f}).matrix()
				;
	});

	poller.on_wheel([&](auto, double delta)
	{
		cam.on_wheel(delta);
	});

	poller.on_mouse_move([&](xsdl::window_t::id_t, geom::point_2i pos, geom::vector_2i delta)
	{
		if (poller.middle_button_down())
		{
			cam.on_rotate(delta);
		}

		mouse = pos;
		update_mouse_proj();
	});

	fps.reset();

	while (!quit)
	{
		poller.poll();

		if (quit) break;

		if (poller.left_button_down())
		{
			water.add(mouse_proj, 0.3f, 0.1f, 0);
		}
		else if (poller.right_button_down())
		{
			water.add(mouse_proj, 0.3f, 0.1f, 1);
		}
		else if (!paused)
		{
			for (int s = 0; s < 3; ++s)
				water.step();
		}

		if (poller.key_down(SDLK_UP))
			light.position[2] += 0.03f;
		if (poller.key_down(SDLK_DOWN))
			light.position[2] -= 0.03f;

		glClearColor(0.9f, 0.95f, 1.f, 0.f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		cam.step();
		update_mouse_proj();

		geom::vector_4f light_pos = geom::homogeneous(light.position);

		if (water_simple)
			caustics.clear();
		else
			caustics.render(light_pos);

		{
			xgl::scope::enable autoname(GL_CULL_FACE);
			xgl::scope::enable autoname(GL_DEPTH_TEST);

			floor.render(projection, cam.transform(), light_pos, cam.position());
			walls.render(projection, cam.transform(), light_pos, cam.position());
		}

		if (show_water)
		{
			xgl::scope::enable autoname(GL_DEPTH_TEST);

			if (water_simple)
				water.render_simple(projection, cam.transform(), light_pos, cam.position());
			else
				water.render(projection, cam.transform(), light_pos, cam.position());
		}

		if (show_light)
		{
			light.render(projection, cam.transform());
		}

		info_text.render(projection_2d);

		if (show_helper && force_show_helper)
		{
			helper_text.render(projection_2d * geom::translation<float, 3>(geom::cast<float>(geom::vector_3i{mouse[0], mouse[1], 0})).homogeneous_matrix());
		}

		window.swap_buffers();

		fps.frame();
		update_info_text();

		xgl::throw_error();
	}
}
