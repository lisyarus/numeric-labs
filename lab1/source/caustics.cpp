#include <water/caustics.hpp>

#include <water/resource.hpp>

#include <xgl/state/viewport.hpp>
#include <xgl/state/enable.hpp>

#include <util/autoname.hpp>

caustics_renderer::caustics_renderer (geom::rectangle_3f const & box)
	: program_(load_program("caustics"))
	, framebuffer_(xgl::framebuffer::create())
	, texture_(xgl::texture_2d::create())
	, box_(box)
{
	geom::rectangle_2i ibox
	{
		geom::cast<int>(box[0]),
		geom::cast<int>(box[1]),
	};

	xgl::scope::bind_texture autoname(texture_);
	texture_.set(GL_TEXTURE_MIN_LOD, 0);
	texture_.set(GL_TEXTURE_MAX_LOD, 8);
	texture_.set(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	texture_.set(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	texture_.load<xgl::pixel_1b>(ibox.lengths() * 64);

	xgl::scope::bind_framebuffer autoname(framebuffer_);
	framebuffer_.attach(texture_);

	if (!framebuffer_.complete())
		throw std::runtime_error("Framebuffer not complete!");
}

void caustics_renderer::render (geom::vector_4f const & light)
{
	{
		xgl::scope::bind_framebuffer autoname(framebuffer_);

		glClearColor(0.0f, 0.f, 0.f, 0.f);
		glClear(GL_COLOR_BUFFER_BIT);

		xgl::scope::set_viewport autoname(texture_.rect());

		xgl::scope::enable autoname(GL_BLEND);
		xgl::scope::disable autoname(GL_DEPTH_TEST);
		xgl::scope::disable autoname(GL_CULL_FACE);
		glBlendFunc(GL_ONE, GL_ONE);

		xgl::scope::bind_program autoname(program_);
		program_["light"] = light;
		program_["origin"] = box_(0.f, 0.f, 0.f);
		program_["size"] = box_.lengths();

		vertex_renderer();
	}

	xgl::scope::bind_texture autoname(texture_);
	glGenerateMipmap(texture_.type());
}

void caustics_renderer::clear ( )
{
	{
		xgl::scope::bind_framebuffer autoname(framebuffer_);

		glClearColor(0.0f, 0.f, 0.f, 0.f);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	xgl::scope::bind_texture autoname(texture_);
	glGenerateMipmap(texture_.type());
}
