#include <water/camera.hpp>

#include <geom/transform/translation.hpp>
#include <geom/transform/scale.hpp>
#include <geom/transform/rotation.hpp>
#include <geom/transform/permutation.hpp>
#include <geom/transform/homogeneous.hpp>
#include <geom/math.hpp>
#include <geom/operations/interval.hpp>

camera::camera ( )
	: a(geom::pi / 4.0f, 0.5f)
	, b(geom::pi * (1.f / 6.f), 0.5f)
	, d(10.f, 0.5f)
{
	target = {0.f, 0.f, 0.f};
}

geom::matrix_4x4f camera::transform ( ) const
{
	return
			geom::matrix_4x4f::identity()
			*	geom::translation<float, 3>({0.f, 0.f, -*d}).homogeneous_matrix()
			*	geom::homogeneous(geom::rotation_3<float>({1.f, 0.f, 0.f}, *b).matrix())
			*	geom::homogeneous(geom::rotation_3<float>({0.f, 1.f, 0.f}, *a).matrix())
			*	geom::scale<float, 4>({1.f, 1.f, -1.f, 1.f}).matrix()
			*	geom::swap<float, 4>(1, 2).matrix()
			*	geom::translation<float, 3>(geom::point_3f::zero() - target).homogeneous_matrix()
			;
}

geom::point_3f camera::position ( ) const
{
	return target + *d * geom::vector_3f
	{
		- std::cos(*b) * std::sin(*a),
		- std::cos(*b) * std::cos(*a),
		std::sin(*b),
	};
}

void camera::on_wheel (float delta)
{
	d.value_target *= std::pow(0.8, delta);
}

void camera::on_rotate (geom::vector_2i delta)
{
	a.value_target += delta[0] * 0.004f;
	b.value_target += delta[1] * 0.004f;

	b.value_target = geom::clamp(b.value_target, {- geom::pi / 2.f, geom::pi / 2.f});
}
