#include <water/resource.hpp>

#include <fstream>
#include <sstream>

std::string load_file (std::string const & name)
{
	std::ifstream ifs(LAB1_RESOURCE_DIR + std::string("/") + name);
	std::ostringstream oss;
	oss << ifs.rdbuf();
	return oss.str();
}

xgl::program load_program (std::string const & name)
{
	return xgl::program::create(
		load_shader<xgl::vertex_shader>(name + ".vs"),
		load_shader<xgl::fragment_shader>(name + ".fs")
	);
}

xgl::program load_transform_program (std::string const & name, std::vector<std::string> const & capture, xgl::enum_t attribs)
{
	auto shader = load_shader<xgl::vertex_shader>(name + ".vs");

	xgl::program program = xgl::program::create();
	std::vector<const char *> capture_str;
	for (std::string const & str : capture)
		capture_str.push_back(str.data());
	glTransformFeedbackVaryings(program.id(), capture_str.size(), capture_str.data(), attribs);
	program.link(shader);

	return program;
}
