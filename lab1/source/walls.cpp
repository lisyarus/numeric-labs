#include <water/walls.hpp>

#include <water/tile.hpp>

#include <vector>
#include <geom/primitives/ndarray.hpp>

static xgl::texture_2d create_wall_texture (std::size_t size, xgl::pixel_4b c1, xgl::pixel_4b c2)
{
	auto texture = xgl::texture_2d::create();

	xgl::scope::bind_texture autoname(texture);
	texture.set(GL_TEXTURE_MIN_LOD, 0);
	texture.set(GL_TEXTURE_MAX_LOD, 4);
	texture.set(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	texture.set(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	texture.set(GL_TEXTURE_MAX_ANISOTROPY_EXT, 4);
	texture.set(GL_TEXTURE_WRAP_S, GL_REPEAT);
	texture.set(GL_TEXTURE_WRAP_T, GL_REPEAT);

	geom::ndarray<xgl::pixel_4b, 2> pixels({size, size});

	for (std::size_t x = 0; x < size; ++x)
	{
		for (std::size_t y = 0; y < size; ++y)
		{
			int dx = static_cast<int>(x) - static_cast<int>(size / 2);
			int dy = static_cast<int>(y) - static_cast<int>(size / 2);

			if (dx*dx + dy*dy < size*size/8)
			{
				pixels.at(x, y) = c1;
			}
			else
			{
				pixels.at(x, y) = c2;
			}
		}
	}

	texture.load({static_cast<int>(size), static_cast<int>(size)}, pixels.data());

	glGenerateMipmap(texture.type());

	return texture;
}

walls_renderer::walls_renderer ( )
{
//	texture_ = create_wall_texture(1024, {0, 255, 0, 255}, {127, 0, 0, 255});
	texture_ = create_tile_texture({127, 255, 255, 255}, {0, 0, 255, 255});
}

void walls_renderer::update (geom::rectangle_3f const & box)
{
	std::vector<vertex> vertices;

	geom::vector_3f tc = box.lengths();

	geom::vector_2f c {0.f, 0.f};

	// x+
	{
		geom::vector_3f n { 1.f, 0.f, 0.f };

		vertices.push_back({ box(0, 0, 0), n, {{0.f, 0.f}} });
		vertices.push_back({ box(0, 1, 0), n, {{tc[1], 0.f}} });
		vertices.push_back({ box(0, 1, 1), n, {{tc[1], tc[2]}} });

		vertices.push_back({ box(0, 0, 0), n, {{0.f, 0.f}} });
		vertices.push_back({ box(0, 1, 1), n, {{tc[1], tc[2]}} });
		vertices.push_back({ box(0, 0, 1), n, {{0.f, tc[2]}} });
	}

	// x-
	{
		geom::vector_3f n { -1.f, 0.f, 0.f };

		vertices.push_back({ box(1, 1, 0), n, {{tc[1], 0.f}} });
		vertices.push_back({ box(1, 0, 0), n, {{0.f, 0.f}} });
		vertices.push_back({ box(1, 0, 1), n, {{0.f, tc[2]}} });

		vertices.push_back({ box(1, 1, 0), n, {{tc[1], 0.f}} });
		vertices.push_back({ box(1, 0, 1), n, {{0.f, tc[2]}} });
		vertices.push_back({ box(1, 1, 1), n, {{tc[1], tc[2]}} });
	}

	// y+
	{
		geom::vector_3f n { 0.f, 1.f, 0.f };

		vertices.push_back({ box(1, 0, 0), n, {{tc[0], 0.f}} });
		vertices.push_back({ box(0, 0, 0), n, {{0.f, 0.f}} });
		vertices.push_back({ box(0, 0, 1), n, {{0.f, tc[2]}} });

		vertices.push_back({ box(1, 0, 0), n, {{tc[0], 0.f}} });
		vertices.push_back({ box(0, 0, 1), n, {{0.f, tc[2]}} });
		vertices.push_back({ box(1, 0, 1), n, {{tc[0], tc[2]}} });
	}

	// y-
	{
		geom::vector_3f n { 0.f, -1.f, 0.f };

		vertices.push_back({ box(0, 1, 0), n, {{0.f, 0.f}} });
		vertices.push_back({ box(1, 1, 0), n, {{tc[0], 0.f}} });
		vertices.push_back({ box(1, 1, 1), n, {{tc[0], tc[2]}} });

		vertices.push_back({ box(0, 1, 0), n, {{0.f, 0.f}} });
		vertices.push_back({ box(1, 1, 1), n, {{tc[0], tc[2]}} });
		vertices.push_back({ box(0, 1, 1), n, {{0.f, tc[2]}} });
	}

	set_data(vertices.data(), vertices.size());
}
