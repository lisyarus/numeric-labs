#include <water/floor.hpp>

#include <water/tile.hpp>

floor_renderer::floor_renderer ( )
{
	texture_ = create_tile_texture({0, 0, 255, 255}, {255, 255, 255, 255});
}

void floor_renderer::update (geom::rectangle_3f const & box)
{
	box_ = box;

	geom::vector_3f n { 0.f, 0.f, 1.f };

	geom::vector_2f tc {
		box[0].length(),
		box[1].length()
	};

	std::array<vertex, 6> vertices = {{
		{ box(0, 0, 0), n, {{0.f, 0.f}} },
		{ box(1, 0, 0), n, {{tc[0], 0.f}} },
		{ box(1, 1, 0), n, {{tc[0], tc[1]}} },

		{ box(0, 0, 0), n, {{0.f, 0.f}} },
		{ box(1, 1, 0), n, {{tc[0], tc[1]}} },
		{ box(0, 1, 0), n, {{0.f, tc[1]}} },
	}};

	set_data(vertices.data(), vertices.size());
}

void floor_renderer::set_uniforms (xgl::program & program)
{
	program["origin"] = box_(0.f, 0.f, 0.f);
	program["size"] = box_.lengths();
}
