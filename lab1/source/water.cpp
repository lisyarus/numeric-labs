#include <water/water.hpp>

#include <water/resource.hpp>

#include <util/autoname.hpp>

#include <xgl/state/enable.hpp>

#include <geom/math.hpp>

water_renderer::water_renderer ( )
	: program_(load_program("water"))
	, simple_program_(load_program("water_simple"))
	, step_program_(load_transform_program("step", {"h", "h_vel"}))
	, normals_program_(load_transform_program("normals", {"n"}))
	, add_program_(load_transform_program("add", {"h", "h_vel"}))
	, array_(xgl::array::create())
	, step_array_(xgl::array::create())
	, position_buffer_(xgl::array_buffer::create())
	, edge_buffer_(xgl::array_buffer::create())
	, height_buffer_(xgl::array_buffer::create())
	, height_buffer_back_(xgl::array_buffer::create())
	, height_vel_buffer_(xgl::array_buffer::create())
	, height_vel_buffer_back_(xgl::array_buffer::create())
	, normal_buffer_(xgl::array_buffer::create())
	, index_buffer_(xgl::element_array_buffer::create())
	, size_(0)
	, sizex_(0)
	, sizey_(0)
{ }

std::function<void()> water_renderer::make_vertex_renderer ( )
{
	return [this]()
	{
		if (index_count() == 0) return;

		array_.draw(GL_TRIANGLES, index_buffer_, index_count(), GL_UNSIGNED_INT);
	};
}

void water_renderer::render (geom::matrix_4x4f const & projection, geom::matrix_4x4f const & modelview, geom::vector_4f const & light, geom::point_3f const & camera)
{
	if (index_count() == 0) return;

	xgl::scope::bind_texture_to_unit autoname(GL_TEXTURE0, *floor_texture);
	xgl::scope::bind_texture_to_unit autoname(GL_TEXTURE1, *wall_texture);
	xgl::scope::bind_texture_to_unit autoname(GL_TEXTURE2, *caustics_texture);

	xgl::scope::bind_program autoname(program_);
	program_["projection"] = projection;
	program_["modelview"] = modelview;
	program_["light"] = light;
	program_["camera"] = camera;
	program_["floor_texture"] = 0;
	program_["wall_texture"] = 1;
	program_["caustics_texture"] = 2;
	program_["origin"] = box_(0.f, 0.f, 0.f);
	program_["size"] = box_.lengths();

	xgl::scope::bind_array autoname(array_);

	{
		xgl::scope::bind_buffer autoname(height_buffer_);
		array_[1].enable().data((float const *)nullptr);
	}

	array_.draw(GL_TRIANGLES, index_buffer_, index_count(), GL_UNSIGNED_INT);
}

void water_renderer::render_simple (geom::matrix_4x4f const & projection, geom::matrix_4x4f const & modelview, geom::vector_4f const & light, geom::point_3f const & camera)
{
	if (index_count() == 0) return;

	xgl::scope::bind_program autoname(simple_program_);
	simple_program_["projection"] = projection;
	simple_program_["modelview"] = modelview;
	simple_program_["light"] = light;
	simple_program_["color"] = geom::vector_4f{0.5f, 0.9f, 1.f, 1.f};

	xgl::scope::bind_array autoname(array_);

	{
		xgl::scope::bind_buffer autoname(height_buffer_);
		array_[1].enable().data((float const *)nullptr);
	}

	array_.draw(GL_TRIANGLES, index_buffer_, index_count(), GL_UNSIGNED_INT);
}

void water_renderer::update (geom::rectangle_3f const & box, GLuint size)
{
	box_ = box;
	size_ = size;
	sizex_ = GLuint(size * box[0].length());
	sizey_ = GLuint(size * box[1].length());

	auto index = [this](GLuint x, GLuint y)
	{
		return x + (sizex_ + 1) * y;
	};

	xgl::scope::bind_array autoname(array_);

	{
		std::vector<GLuint> indices;
		indices.reserve(index_count());

		for (GLuint y = 0; y < sizey_; ++y)
		{
			for (GLuint x = 0; x < sizex_; ++x)
			{
				indices.push_back(index(x, y));
				indices.push_back(index(x + 1, y));
				indices.push_back(index(x + 1, y + 1));

				indices.push_back(index(x, y));
				indices.push_back(index(x + 1, y + 1));
				indices.push_back(index(x, y + 1));
			}
		}

		index_buffer_.data(indices);
	}

	{
		struct static_vertex
		{
			geom::point_2f position;
		};

		std::vector<static_vertex> static_data;

		for (GLuint y = 0; y <= sizey_; ++y)
		{
			for (GLuint x = 0; x <= sizex_; ++x)
			{
				auto p = box(float(x) / sizex_, float(y) / sizey_, 0.f);
				static_data.push_back({{ {p[0], p[1]} }});
			}
		}

		xgl::scope::bind_buffer autoname(position_buffer_);
		position_buffer_.data(static_data);

		array_[0].enable().data((geom::point_2f const *)nullptr, sizeof(static_vertex));
	}

	{
		std::vector<unsigned char> edge_data;

		for (GLuint y = 0; y <= sizey_; ++y)
		{
			for (GLuint x = 0; x <= sizex_; ++x)
			{
				if (x == 0 || x == sizex_ || y == 0 || y == sizey_)
					edge_data.push_back(0);
				else
					edge_data.push_back(1);
			}
		}

		edge_buffer_.data(edge_data);
	}

	clear();

	{
		xgl::scope::bind_buffer autoname(height_buffer_);
		array_[1].enable().data((float const *)nullptr);
	}
	{
		xgl::scope::bind_buffer autoname(normal_buffer_);
		array_[2].enable().data((geom::vector_3f const *)nullptr);
	}
}

void water_renderer::step ( )
{
	{
		xgl::scope::enable autoname(GL_RASTERIZER_DISCARD);
		xgl::scope::bind_program autoname(step_program_);

		step_program_["dx"] = 2.f / size_;
		step_program_["dt"] = 0.1f;
		step_program_["middle"] = box_[2](0.5f);

		std::size_t total_size = (sizex_ + 1) * (sizey_ - 1) - 3;
		glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, height_buffer_back_.id(), (sizex_ + 2) * sizeof(float), total_size * sizeof(float));
		glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 1, height_vel_buffer_back_.id(), (sizex_ + 2) * sizeof(float), total_size * sizeof(float));

		glBeginTransformFeedback(GL_POINTS);

		xgl::scope::bind_array autoname(step_array_);
		{
			xgl::scope::bind_buffer autoname(height_buffer_);
			step_array_[0].enable().data((float const *)(nullptr) + sizex_ + 2);

			step_array_[2].enable().data((float const *)(nullptr) + sizex_ + 1);
			step_array_[3].enable().data((float const *)(nullptr) + sizex_ + 3);

			step_array_[4].enable().data((float const *)(nullptr) + 1);
			step_array_[5].enable().data((float const *)(nullptr) + 2 * sizex_ + 3);
		}
		{
			xgl::scope::bind_buffer autoname(height_vel_buffer_);
			step_array_[1].enable().data((float const *)(nullptr) + sizex_ + 2);
		}
		{
			xgl::scope::bind_buffer autoname(edge_buffer_);
			step_array_[6].enable().data((unsigned char const *)(nullptr) + sizex_ + 2);
		}

		glDrawArrays(GL_POINTS, 0, total_size);

		glEndTransformFeedback();

	}

	std::swap(height_buffer_, height_buffer_back_);
	std::swap(height_vel_buffer_, height_vel_buffer_back_);

	update_normals();
}

void water_renderer::add (geom::point_2f const & center, float radius, float intensity, int mode)
{
	{
		xgl::scope::enable autoname(GL_RASTERIZER_DISCARD);
		xgl::scope::bind_program autoname(add_program_);

		add_program_["center"] = center;
		add_program_["radius"] = radius;
		add_program_["intensity"] = intensity;
		add_program_["middle"] = box_[2](0.5f);
		add_program_["mode"] = float(mode);

		std::size_t total_size = (sizex_ + 1) * (sizey_ + 1);
		glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, height_buffer_back_.id(), 0, total_size * sizeof(float));
		glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 1, height_vel_buffer_back_.id(), 0, total_size * sizeof(float));

		glBeginTransformFeedback(GL_POINTS);

		xgl::scope::bind_array autoname(step_array_);
		{
			xgl::scope::bind_buffer autoname(height_buffer_);
			step_array_[0].enable().data((float const *)(nullptr));
		}
		{
			xgl::scope::bind_buffer autoname(height_vel_buffer_);
			step_array_[1].enable().data((float const *)(nullptr));
		}
		{
			xgl::scope::bind_buffer autoname(position_buffer_);
			step_array_[2].enable().data((geom::vector_2f const *)(nullptr));
		}
		{
			xgl::scope::bind_buffer autoname(edge_buffer_);
			step_array_[3].enable().data((unsigned char const *)(nullptr));
		}

		glDrawArrays(GL_POINTS, 0, total_size);

		glEndTransformFeedback();

	}

	std::swap(height_buffer_, height_buffer_back_);
	std::swap(height_vel_buffer_, height_vel_buffer_back_);

	update_normals();
}

void water_renderer::clear ( )
{
	std::vector<float> height_data;
	std::vector<float> height_vel_data;

	for (GLuint y = 0; y <= sizey_; ++y)
	{
		for (GLuint x = 0; x <= sizex_; ++x)
		{
			height_data.push_back(box_[2](0.5f));

			height_vel_data.push_back(0.f);
		}
	}

	height_buffer_.data(height_data);
	height_vel_buffer_.data(height_vel_data);

	height_buffer_back_.data(height_data);
	height_vel_buffer_back_.data(height_vel_data);

	{
		std::vector<geom::vector_3f> normal_data;
		for (GLuint y = 0; y <= sizey_; ++y)
		{
			for (GLuint x = 0; x <= sizex_; ++x)
			{
				normal_data.push_back(geom::vector_3f{0.f, 0.f, 1.f});
			}
		}

		normal_buffer_.data(normal_data);
	}
}

void water_renderer::load_steady_state (int sx, int sy)
{
	std::vector<float> height_data;

	for (GLuint y = 0; y <= sizey_; ++y)
	{
		for (GLuint x = 0; x <= sizex_; ++x)
		{
			height_data.push_back(box_[2](0.5f) + box_[2].length() * 0.2f * std::sin(float(x) / sizex_ * sx * geom::pi) * std::sin(float(y) / sizey_ * sy * geom::pi));
		}
	}

	height_buffer_.data(height_data);
}

std::size_t water_renderer::index_count ( )
{
	return sizex_ * sizey_ * 6;
}

std::size_t water_renderer::vertex_count ( )
{
	return (sizex_ + 1) * (sizey_ + 1);
}

void water_renderer::update_normals ( )
{
	xgl::scope::enable autoname(GL_RASTERIZER_DISCARD);
	xgl::scope::bind_program autoname(normals_program_);

	std::size_t total_size = (sizex_ + 1) * (sizey_ - 1) - 3;
	glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, normal_buffer_.id(), (sizex_ + 2) * sizeof(geom::vector_3f), total_size * sizeof(geom::vector_3f));

	glBeginTransformFeedback(GL_POINTS);

	xgl::scope::bind_array autoname(step_array_);
	{
		xgl::scope::bind_buffer autoname(height_buffer_);
		step_array_[0].enable().data((float const *)(nullptr) + sizex_ + 2);

		step_array_[2].enable().data((float const *)(nullptr) + sizex_ + 1);
		step_array_[4].enable().data((float const *)(nullptr) + sizex_ + 3);

		step_array_[6].enable().data((float const *)(nullptr) + 1);
		step_array_[8].enable().data((float const *)(nullptr) + 2 * sizex_ + 3);
	}
	{
		xgl::scope::bind_buffer autoname(position_buffer_);
		step_array_[1].enable().data((geom::vector_2f const *)(nullptr) + sizex_ + 2);

		step_array_[3].enable().data((geom::vector_2f const *)(nullptr) + sizex_ + 1);
		step_array_[5].enable().data((geom::vector_2f const *)(nullptr) + sizex_ + 3);

		step_array_[7].enable().data((geom::vector_2f const *)(nullptr) + 1);
		step_array_[9].enable().data((geom::vector_2f const *)(nullptr) + 2 * sizex_ + 3);
	}
	{
		xgl::scope::bind_buffer autoname(edge_buffer_);
		step_array_[10].enable().data((unsigned char const *)(nullptr) + sizex_ + 2);
	}

	glDrawArrays(GL_POINTS, 0, total_size);

	glEndTransformFeedback();
}
