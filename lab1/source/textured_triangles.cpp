#include <water/textured_triangles.hpp>

#include <water/resource.hpp>

#include <util/autoname.hpp>

#include <optional>

textured_triangles_renderer::textured_triangles_renderer ( )
	: program_(load_program("textured"))
	, array_(xgl::array::create())
	, buffer_(xgl::array_buffer::create())
	, count_(0)
{
	caustics_texture = nullptr;
}

void textured_triangles_renderer::render (geom::matrix_4x4f const & projection, geom::matrix_4x4f const & modelview, geom::vector_4f const & light, geom::point_3f const & camera)
{
	if (count_ == 0) return;

	xgl::scope::bind_texture_to_unit autoname(GL_TEXTURE0, texture_);
	std::optional<xgl::scope::bind_texture_to_unit> bind_caustics;

	xgl::scope::bind_program autoname(program_);
	program_["projection"] = projection;
	program_["modelview"] = modelview;
	program_["light"] = light;
	program_["texture"] = 0;

	if (caustics_texture)
	{
		bind_caustics.emplace(GL_TEXTURE1, *caustics_texture);
		program_["caustics_texture"] = 1;
		program_["use_caustics"] = true;
	}
	else
		program_["use_caustics"] = false;

	set_uniforms(program_);

	array_.draw(GL_TRIANGLES, count_);
}

void textured_triangles_renderer::set_data (vertex const * data, std::size_t size)
{
	xgl::scope::bind_buffer autoname(buffer_);
	buffer_.data(data, size);
	count_ = size;

	xgl::scope::bind_array autoname(array_);
	array_[0].enable().data((geom::point_3f const *)nullptr, sizeof(vertex));
	array_[1].enable().data((geom::vector_3f const *)(nullptr) + 1, sizeof(vertex));
	array_[2].enable().data((geom::vector_2f const *)((float *)(nullptr) + 6), sizeof(vertex));
}
