#ifdef LAB1_USE_TEXT

#include <water/text.hpp>
#include <water/resource.hpp>

#include <xgl/text/freetype.hpp>
#include <xgl/state/enable.hpp>

#include <util/autoname.hpp>
#include <util/unicode.hpp>

text_renderer::text_renderer ( )
	: program_(load_program("text"))
{
	xgl::text::freetype::library ft_lib;
	atlas_ = ft_lib.load(XGL_DEMO_FONT, 0, 16, 16);
}

void text_renderer::update (std::string const & text, geom::point_2f const & start)
{
	xgl::text::options opts;
	opts.start = start;
	opts.line_spacing = 1.2f;
	opts.size = 16.f;
	mesh_.update(atlas_, util::from_utf8(text), opts);
}

void text_renderer::render (geom::matrix_4x4f const & projection)
{
	xgl::scope::enable autoname(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	xgl::scope::bind_program autoname(program_);
	program_["projection"] = projection;
	program_["atlas"] = 0;
	program_["color"] = geom::vector_4f{0.f, 0.f, 0.f, 1.f};

	xgl::scope::bind_texture_to_unit autoname(GL_TEXTURE0, atlas_.texture);

	mesh_.render();
}

#endif
