#include <water/tile.hpp>

#include <vector>

xgl::texture_2d create_tile_texture (xgl::pixel_4b c1, xgl::pixel_4b c2)
{
	auto texture = xgl::texture_2d::create();

	xgl::scope::bind_texture autoname(texture);
	texture.set(GL_TEXTURE_MIN_LOD, 0);
	texture.set(GL_TEXTURE_MAX_LOD, 4);
	texture.set(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	texture.set(GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	texture.set(GL_TEXTURE_MAX_ANISOTROPY_EXT, 4);
	texture.set(GL_TEXTURE_WRAP_S, GL_REPEAT);
	texture.set(GL_TEXTURE_WRAP_T, GL_REPEAT);

	int n = 8;
	int l = 14;
	int size = n * (l+2);

	std::vector<xgl::pixel_4b> pixels(size * size, c1);

	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			int x1 = i * (l+2);
			int x2 = x1 + l + 1;;

			pixels[j * size + x1] = c2;
			pixels[x1 * size + j] = c2;
			pixels[j * size + x2] = c2;
			pixels[x2 * size + j] = c2;
		}
	}

	texture.load({size, size}, pixels.data());

	glGenerateMipmap(texture.type());

	return texture;
}
