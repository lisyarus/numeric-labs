#include <water/light.hpp>

#include <water/resource.hpp>

#include <geom/transform/translation.hpp>
#include <geom/transform/scale.hpp>
#include <geom/transform/homogeneous.hpp>

#include <util/autoname.hpp>

#include <vector>

light_renderer::light_renderer ( )
	: program_(load_program("simple"))
	, array_(xgl::array::create())
	, buffer_(xgl::array_buffer::create())
{
	std::vector<geom::vector_3f> dir;

	dir.push_back({  1.f,  0.f,  0.f });
	dir.push_back({  0.f,  1.f,  0.f });
	dir.push_back({  0.f,  0.f,  1.f });

	dir.push_back({  1.f,  1.f,  0.f });
	dir.push_back({  1.f, -1.f,  0.f });
	dir.push_back({  1.f,  0.f,  1.f });
	dir.push_back({  1.f,  0.f, -1.f });
	dir.push_back({  0.f,  1.f,  1.f });
	dir.push_back({  0.f,  1.f, -1.f });

	dir.push_back({  1.f,  1.f,  1.f });
	dir.push_back({  1.f, -1.f,  1.f });
	dir.push_back({  1.f,  1.f, -1.f });
	dir.push_back({  1.f, -1.f, -1.f });

	std::vector<geom::vector_3f> points;

	for (auto const & v : dir)
	{
		points.push_back(-normalized(v));
		points.push_back(normalized(v));
	}

	xgl::scope::bind_buffer autoname(buffer_);
	buffer_.data(points);

	array_[0].enable().data((geom::vector_3f const *)(nullptr));
}

void light_renderer::render (geom::matrix_4x4f const & projection, geom::matrix_4x4f const & modelview)
{
	xgl::scope::bind_program autoname(program_);
	program_["transform"] = projection * modelview
		* geom::translation<float, 3>(position - geom::point_3f::zero()).homogeneous_matrix()
		* geom::homogeneous(geom::scale<float, 3>(size).matrix())
		;
	program_["color"] = geom::vector_4f{ 1.f, 1.f, 0.f, 1.f };

	array_.draw(GL_LINES, 26);
}
